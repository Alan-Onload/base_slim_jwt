<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\UploadedFileInterface;
use Firebase\JWT\JWT;

require __DIR__ . '/../Config/ConfigApp.php';
require __DIR__ . '/../banco/Database.php';



$dados = file_get_contents(__DIR__ . "/../banco/dados.json");

class HomeController extends ConfigApp
{

    public function login(Request $req, Response $res, array $args): Response
    {
        $body = $req->getParsedBody();
        $now_time = time();
        if ($body["username"] === "admin" and $body["password"] === "123") {
            $payload = array(
                "alg" => "HS256",
                "iss" => "admin@gmail.com",
                "iat" => $now_time,
                "exp" => $now_time + (60 * 1200),
                "uid" => 1
            );

            $jwt = JWT::encode($payload, $this->getACCESS_KEY(), "HS256");
            $token = ["token" => $jwt];
            $res->getBody()->write(json_encode(["auth" => true, "token" => $token]), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            return $res->withHeader("Content-Type", "application/json")->withStatus(201);
        } else {
            # code...
            $res->getBody()->write(json_encode(["auth" => false, "token" => null]), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            return $res->withHeader("Content-Type", "application/json")->withStatus(500);
        }
    }

    public function listar(Request $req, Response $res, array $args): Response
    {
        $cnx = new DBConnection();
        $pdo = $cnx->connect();
        $pstm = $pdo->prepare("SELECT * FROM produtos");
        $pstm->execute();
        $result = $pstm->fetchAll(PDO::FETCH_ASSOC);
        $res->getBody()->write(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        return $res->withHeader("Content-Type", "application/json")->withStatus(200);
    }

    public function pesquisar(Request $req, Response $res, array $args): Response
    {

        $cnx = new DBConnection();
        $pdo = $cnx->connect();
        $pstm = $pdo->prepare("SELECT * FROM produtos WHERE id=:id");
        $pstm->bindParam(":id", $args["id"], PDO::PARAM_INT);
        $pstm->execute();
        $result = $pstm->fetchObject();
        $res->getBody()->write(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        return $res->withHeader("Content-Type", "application/json")->withStatus(200);
    }

    public function salvar(Request $request, Response $response, array $args): Response
    {

        $body = $request->getParsedBody();
        $sql = "INSERT INTO produtos(data,imagem,nome,preco,descricao) VALUES (:data,:imagem,:nome,:preco,:descricao);";
        $directory = __DIR__ . '/../imagens';
        $uploadedFiles = $request->getUploadedFiles();


        $uploadedFile = $uploadedFiles['imagem'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = $this->moveUploadedFile($directory, $uploadedFile);
            $cnx = new DBConnection();
            $pdo = $cnx->connect();
            $pstm = $pdo->prepare($sql);
            $pstm->bindParam(":data", date('d/m/Y'));
            $pstm->bindParam(":imagem", $filename);
            $pstm->bindParam(":nome", $body['nome']);
            $pstm->bindParam(":preco", $body['preco']);
            $pstm->bindParam(":descricao", $body['descricao']);
            $result = $pstm->execute();
            $pdo = null;
            $response->getBody()->write(json_encode(["msg" => $result]));
        }
        return $response->withHeader("Content-Type", "application/json")->withStatus(200);
    }

    function moveUploadedFile(string $directory, UploadedFileInterface $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);

        $basename = bin2hex(random_bytes(8));
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }
}
