<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Routing\RouteCollectorProxy;
use Slim\Factory\AppFactory;
use Slim\Psr7\Response;
use JWT_ESTUDO\SQLiteConnection;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/Controller/HomeController.php';
require __DIR__ . '/Midleware/Autenticacao.php';



$ctrl = new HomeController();


$app = AppFactory::create();
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);




$app->add(function(Request $request,RequestHandler $handler){

    $response = $handler->handle($request);
    return $response->withHeader('Access-Control-Allow-Origin', '*')
    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

});


$app->group("/admin",function(RouteCollectorProxy $group){
$group->post('/',HomeController::class . ":login");
});


$app->group("/api",function(RouteCollectorProxy $group){
$group->get('/',HomeController::class . ":listar");
$group->get('/{id}',HomeController::class . ":pesquisar");
$group->post('/',HomeController::class . ":salvar");
})->add(new Autenticacao());

// Run app
$app->run();
