<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

require_once __DIR__ . "/../Config/ConfigApp.php";

final class Autenticacao extends ConfigApp{


    public function __invoke(Request $request,RequestHandlerInterface $handler)
    {
        $token = str_replace("Bearer ","",(string) $request->getHeaderLine("Authorization"));
        try{
            $jwt = JWT::decode($token,new Key($this->getACCESS_KEY(),"HS256"));
        if (!$token) {
            $response = new Response();
            $response->getBody()->write(json_encode(["auth"=>false,"token"=>null]));
            return $response;
        }else{
            $response = $handler->handle($request);
            return $response;
        }
        }catch(Exception $ex){
            $erros = array();
            array_push($erros,["err"=>$ex->getMessage()]);
            $response = new Response();
            $response->getBody()->write(json_encode(["auth"=>false,"token"=>null,"erro"=>$erros],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            return $response;

        }

        
    }


}