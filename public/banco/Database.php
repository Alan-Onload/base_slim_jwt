<?php

/**
 * SQLite connnection
 */
final class DBConnection {
    /**
     * PDO instance
     * @var type 
     */
    private $pdo;

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect() {
        $sql = "CREATE TABLE IF NOT EXISTS produtos (
            id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            data	TEXT NOT NULL,
            imagem	TEXT NOT NULL,
            nome	TEXT NOT NULL,
            preco	REAL NOT NULL,
            descricao	TEXT NOT NULL
        )";
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . __DIR__ . "/../db/onload.db");
            $this->pdo->exec($sql);
        }
       
        return $this->pdo;
    }
}